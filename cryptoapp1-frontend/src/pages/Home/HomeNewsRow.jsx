import React from 'react'
import moment from 'moment';

function HomeNewsRow(props) {
    const {index, row} = props;
    console.log('news',row)
    return (
        <>
        <div class="col mb-2 d-flex align-items-center justify-content-center">
            <div class="max-w-min pt-2">
            {(row.image)
            ? (
                <>
                <div class=" w-100 h-50 d-flex align-items-center justify-center">
                    <img src={row.image.thumbnail.contentUrl} class="" alt="" width="180" />
                </div>
                </>
            ):(
                <>
                <div class=" w-100 h-50 ">
                <img src='default-news.png' class="card-img-top img-fluid" alt=""/>
                </div>
                </>
            )

            }
            <div class="card-inner rounded-sm bg-secondary">
                <a  href={row.url} target="_blank">
                    <p class=" fw-bold text-white none-decorate">{row.description.substring(0,50)}...
                    </p>
                </a>
                <p class=" mt-1 px-2 row text-dark  f-flex flex-row text-primary ">
                    {moment(row.datePublished).fromNow()}
                </p>
            </div>
            
            </div>
        </div>


        </>
    )
}

export default HomeNewsRow
