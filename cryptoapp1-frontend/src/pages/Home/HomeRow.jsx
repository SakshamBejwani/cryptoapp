import React from 'react'
import {Link} from 'react-router-dom'
function HomeRow(props) {
    const {index, row} = props;
    
    return (
        
        <tr className="min-w-max">
            <td className="h-100 text-white"><strong class="price fw-bold">{index + 1}</strong></td>
            <td>
                <Link to={"/coin/"+row.symbol}><strong class="none-decorate text-white price">{row.name}    {row.symbol}</strong></Link>
            </td>
            {/* <td className="text-center text-white">{row.symbol}</td> */}
            <td className=" text-white">${parseFloat(row.quote.USD.price).toFixed(6)}</td>
            {(parseFloat(row.quote.USD.percent_change_24h)>0) ? (
                 <>
                    <td class="font-green  price fw-bold">+{parseFloat(row.quote.USD.percent_change_24h).toFixed(2)}%</td>
                </>
            ):(
                <>
                 <td class="font-orange price fw-bold">{parseFloat(row.quote.USD.percent_change_24h).toFixed(2)}%</td>
                </>
            )}
            {(parseFloat(row.quote.USD.percent_change_7d)>0) ? (
                 <>
                    <td class="font-green  price fw-bold">+{parseFloat(row.quote.USD.percent_change_7d).toFixed(2)}%</td>
                </>
            ):(
                <>
                 <td class="font-orange price fw-bold">{parseFloat(row.quote.USD.percent_change_7d).toFixed(2)}%</td>
                </>
            )}
            
            {/* <td class="font-orange text-center">{parseFloat(row.quote.USD.percent_change_7d).toFixed(2)}%</td> */}
            <td className="text-white price">{row.quote.USD.market_cap.toFixed(0)}</td>
            <td className=" text-white price">{row.quote.USD.volume_24h.toFixed(0)} {row.symbol}</td>
            
        </tr>
                                        
            
    )
}

export default HomeRow
