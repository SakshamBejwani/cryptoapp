const url = 'https://cryptoapp-server.vercel.app'

export const getCoinMetaData=(crypto, callback )=>{
    console.log(crypto)
    fetch(url+'/cryptocurrency/info/'+crypto)
    .then((response)=>callback(response)) 
    .catch((error)=>{console.log(error)})

}
export const getSpecificCoinMetaData=( callback )=>{

    fetch(url + '/cryptocurrency/latest')
    .then((response)=>callback(response)) 
    .catch((error)=>{console.log(error)})

}
export const getGlobalStats=( callback )=>{

    fetch(url+'/global-metrics/quotes/latest' )
    .then((response)=>callback(response)) 
    .catch((error)=>{console.log(error)})

}
export const getCoinQuote=(crypto, callback )=>{

    fetch(url+'/cryptocurrency/quote/' + crypto )
    .then((response)=>callback(response)) 
    .catch((error)=>{console.log(error)})

}
